package nl.utwente.di.victor.temperatureTranslator;

public class Calculator {

    public double calculate(String temperature) {
        double celsius = Double.parseDouble(temperature);
        return (celsius*1.8)+32;
    }
}
