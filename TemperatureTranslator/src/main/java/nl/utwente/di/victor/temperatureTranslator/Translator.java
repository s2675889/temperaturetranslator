package nl.utwente.di.victor.temperatureTranslator;

import nl.utwente.di.victor.temperatureTranslator.Calculator;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class Translator extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Translator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("temperature") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                Double.toString(calculator.calculate(request.getParameter("temperature"))) +
                "</BODY></HTML>");
    }


}
